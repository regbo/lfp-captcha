package com.lfp.captcha.service;

import java.io.File;

import com.lfp.joe.net.proxy.Proxy;

public interface ImageCaptcha {

	default String imageSolve(File file) {
		return imageSolve(file, null);
	}

	String imageSolve(File file, Proxy proxy);
}
