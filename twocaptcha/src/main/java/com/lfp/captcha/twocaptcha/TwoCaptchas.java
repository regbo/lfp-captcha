package com.lfp.captcha.twocaptcha;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.time.Duration;

import org.apache.commons.lang3.Validate;

import com.lfp.captcha.twocaptcha.config.TwoCaptchaConfig;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.io.FileExt;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.http.client.HttpClients;
import com.lfp.joe.net.http.request.HttpRequests;
import com.lfp.joe.net.status.StatusCodes;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;
import com.twocaptcha.TwoCaptcha;

public class TwoCaptchas {

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final int VERSION = 0;
	private static final MemoizedSupplier<String> FIND_RE_CAPTCHA_PARAMS_FUNCTION_S = MemoizedSupplier.create(() -> {
		var script = Utils.Files.tempFile(THIS_CLASS, VERSION, "findReCaptchaParams", "script.js");
		try {
			if (!script.exists() || script.lastModifiedElapsed(Duration.ofDays(3)))
				updateScript(script);
			return script.inputStream().reader().readAll();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	});

	public static String findReCaptchaParamsFunction() {
		return FIND_RE_CAPTCHA_PARAMS_FUNCTION_S.get();
	}

	public static String findReCaptchaParamsScript() {
		return Streams.of(findReCaptchaParamsFunction(), "return findRecaptchaClients();")
				.joining(MachineConfig.getDefaultLineSeperator());
	}

	public static TwoCaptcha createSolver() {
		var apiKey = Configs.get(TwoCaptchaConfig.class).apiKey();
		Validate.notBlank(apiKey);
		return new TwoCaptcha(apiKey);
	}

	private static void updateScript(FileExt script) throws IOException {
		var url = "https://gist.githubusercontent.com/2captcha/2ee70fa1130e756e1693a5d4be4d8c70/raw/0ad83798e2a5145bebf206c92a51435024bd8122/find-recaptcha-params.js";
		var request = HttpRequests.request().uri(URI.create(url));
		var response = HttpClients.send(request);
		try (var is = response.body(); var fos = new FileOutputStream(script)) {
			StatusCodes.validate(response.statusCode(), 2);
			is.transferTo(fos);
		}
	}

	public static void main(String[] args) {
		System.out.println(findReCaptchaParamsFunction());
	}

}
